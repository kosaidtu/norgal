#!/usr/bin/env python

from __future__ import print_function
import operator

def sortfasta(myfile="scaffold.fa",outfile="longest.fa"):
 n = 10

 with open(myfile) as fid:
  d = {}
  seq = 0
  sseq = ""
  entry = fid.readline().rstrip()[1:]
  line = fid.readline().rstrip()
  
  while line:
   if line[0] == ">":
    d[entry] = [seq,sseq]
    seq = 0
    sseq = ""
    entry = line[1:]
   else:
    seq += len(line)
    sseq += line
   line = fid.readline().rstrip()
  else:
   d[entry] = [seq,sseq]
 sorted_d = sorted(d.items(), key=operator.itemgetter(1),reverse=True)
 a = ">{0}\n{1}\n".format(sorted_d[0][0],sorted_d[0][1][1])
 with open(outfile,"w") as fout:
  fout.write(a)

if __name__ == "__main__":
 import sys
 sortfasta(sys.argv[1],sys.argv[2])

